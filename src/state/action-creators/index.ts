import { ActionType } from "../action-types";
import { Dispatch } from "redux";
import posts from "../../apis/posts";
import { AxiosResponse } from "axios";
import { Post, Posts } from "../reducers/postReducer";
import { IFormInputs } from "../../app.interface";

export const signIn = (userId: string) => {
  return {
    type: ActionType.SIGN_IN,
    payload: userId,
  };
};

export const signOut = () => {
  return {
    type: ActionType.SIGN_OUT,
  };
};

export const fetchPosts = () => async (dispatch: Dispatch<any>) => {
  dispatch({
    type: ActionType.FETCH_POSTS,
  });

  try {
    const response: AxiosResponse<Post[]> = await posts.get("/posts");

    dispatch({
      type: ActionType.FETCH_POSTS_SUCCESS,
      payload: response.data,
    });
  } catch (err: any) {
    dispatch({
      type: ActionType.FETCH_POSTS_FAIL,
      payload: err.message,
    });
  }
};

export const fetchPost = (id: number) => async (dispatch: Dispatch<any>) => {
  dispatch({
    type: ActionType.FETCH_POST,
  });

  try {
    const response: AxiosResponse<Post> = await posts.get(`/posts/${id}`);

    dispatch({
      type: ActionType.FETCH_POST_SUCCESS,
      payload: response.data,
    });
  } catch (err: any) {
    dispatch({
      type: ActionType.FETCH_POST_FAIL,
      payload: err.message,
    });
  }
};

export const editPost =
  (id: number, formValues: IFormInputs) => async (dispatch: Dispatch<any>) => {
    dispatch({
      type: ActionType.EDIT_POST,
    });

    try {
      const response: AxiosResponse<Post> = await posts.patch(
        `/posts/${id}`,
        formValues
      );

      dispatch({ type: ActionType.EDIT_POST_SUCCESS, payload: response.data });
    } catch (err: any) {
      dispatch({
        type: ActionType.EDIT_POST_FAIL,
        payload: err.message,
      });
    }
  };

export const createPost =
  (formValues: IFormInputs) => async (dispatch: Dispatch<any>) => {
    dispatch({
      type: ActionType.CREATE_POST,
    });

    try {
      const response: AxiosResponse<Post> = await posts.post(
        `/posts`,
        formValues
      );

      dispatch({
        type: ActionType.CREATE_POST_SUCCESS,
        payload: response.data,
      });
    } catch (err: any) {
      dispatch({
        type: ActionType.CREATE_POST_FAIL,
        payload: err.message,
      });
    }
  };

export const deletePost =
  (deletedId: number) => async (dispatch: Dispatch<any>) => {
    dispatch({ type: ActionType.DELETE_POST });
    try {
      await posts.delete(`/posts/${deletedId}`);
      dispatch({
        type: ActionType.DELETE_POST_SUCCESS,
        payload: deletedId,
      });
    } catch (e) {
      dispatch({ type: ActionType.DELETE_POST_FAIL });
    }
  };
