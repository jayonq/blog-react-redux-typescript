import { ActionType } from "../action-types";
import { Post, Posts } from "../reducers/postReducer";

// FETCH POSTS

interface FetchPosts {
  type: ActionType.FETCH_POSTS;
}

interface FetchPostsSuccess {
  type: ActionType.FETCH_POSTS_SUCCESS;
  payload: Posts;
}

interface FetchPostsFail {
  type: ActionType.FETCH_POSTS_FAIL;
}

interface FetchPost {
  type: ActionType.FETCH_POST;
}

interface FetchPostSuccess {
  type: ActionType.FETCH_POST_SUCCESS;
  payload: Post;
}

interface FetchPostFail {
  type: ActionType.FETCH_POST_FAIL;
}

interface EditPosts {
  type: ActionType.EDIT_POST;
}

interface EditPostsSuccess {
  type: ActionType.EDIT_POST_SUCCESS;
  payload: Post;
}

interface EditPostsFail {
  type: ActionType.EDIT_POST_FAIL;
}

interface CreatePost {
  type: ActionType.CREATE_POST;
}

interface CreatePostSuccess {
  type: ActionType.CREATE_POST_SUCCESS;
  payload: Post;
}

interface CreatePostFail {
  type: ActionType.CREATE_POST_FAIL;
}

interface DeletePost {
  type: ActionType.DELETE_POST;
}

interface DeletePostSuccess {
  type: ActionType.DELETE_POST_SUCCESS;
  payload: number;
}

interface DeletePostFail {
  type: ActionType.DELETE_POST_FAIL;
}

export type Action =
  | FetchPosts
  | FetchPostsSuccess
  | FetchPostsFail
  | FetchPost
  | FetchPostSuccess
  | FetchPostFail
  | EditPosts
  | EditPostsSuccess
  | EditPostsFail
  | CreatePost
  | CreatePostSuccess
  | CreatePostFail
  | DeletePost
  | DeletePostSuccess
  | DeletePostFail;
