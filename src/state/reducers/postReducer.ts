import _ from "lodash";
import { ActionType } from "../action-types";
import { Action } from "../actions";

export interface Post {
  id: number;
  title: string;
  description: string;
  postcontent: string;
  userId: string;
}

export interface Posts {
  [id: number]: Post;
}

export interface PostsState {
  items: Posts;
  loading: boolean;
  error: String | null;
}

const initialState = {
  items: [],
  loading: false,
  error: null,
};

export default (state: PostsState = initialState, action: Action) => {
  switch (action.type) {
    case ActionType.FETCH_POSTS:
    case ActionType.FETCH_POST:
    case ActionType.EDIT_POST:
    case ActionType.CREATE_POST:
      return { ...state, loading: true };
    case ActionType.FETCH_POSTS_FAIL:
    case ActionType.FETCH_POST_FAIL:
    case ActionType.EDIT_POST_FAIL:
      return { ...state, loading: false };

    case ActionType.CREATE_POST_SUCCESS:
    case ActionType.FETCH_POST_SUCCESS:
      const { id } = action.payload;
      return {
        ...state,
        items: { ...state.items, [id]: action.payload },
        loading: false,
      };

    case ActionType.FETCH_POSTS_SUCCESS:
      return {
        ...state,
        items: { ...state.items, ..._.mapKeys(action.payload, "id") },
        loading: false,
      };
    case ActionType.DELETE_POST_SUCCESS:
      return {
        ...state,
        items: { ..._.omit(state.items, action.payload) },
      };
    default:
      return state;
  }
};
