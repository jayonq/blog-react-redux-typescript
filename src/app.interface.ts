export interface IFormInputs {
  title: string;
  description: string;
  postcontent: string;
}
