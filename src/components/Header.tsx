import { Link } from "react-router-dom";
import GoogleAuth from "./GoogleAuth";

const Header = () => {
  return (
    // <div className="ui secondary pointing menu">
    //   <Link to="/" className="item">
    //     Streamer
    //   </Link>
    //   <div className="right menu">
    //     <Link to="/" className="item">
    //       All Streams
    //     </Link>
    //     <GoogleAuth />
    //   </div>
    // </div>
    <header className="header">
      <div className="header-inner">
        <div className="header__main">
          <Link to="/" className="logo" />
          <nav className="menu">
            <ul className="menu__list">
              <li className="menu__item">
                <Link to="/" className="menu__link">
                  Categories
                </Link>
              </li>
              <li className="menu__item">
                <Link to="/" className="menu__link">
                  Search
                </Link>
              </li>
              <li className="menu__item">
                <Link to="/" className="menu__link">
                  A-plus business
                </Link>
              </li>
            </ul>
          </nav>
        </div>

        {/* <GoogleAuth /> */}
      </div>
    </header>
  );
};

export default Header;
