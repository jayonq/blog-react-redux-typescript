import "../css/App.css";
import Header from "./Header";
import { Route, Routes } from "react-router-dom";
import PostList from "../components/posts/PostList";
import PostEdit from "./posts/PostEdit";
import PostCreate from "./posts/PostCreate";
import PostDelete from "./posts/PostDelete";
import PostShow from "./posts/PostShow";

const App = () => {
  return (
    <>
      <div>
        <Header />
        <Routes>
          <Route path="/" element={<PostList />} />
          <Route path="/posts/new" element={<PostCreate />} />
          <Route path="/posts/edit/:id" element={<PostEdit />} />
          <Route path="/posts/delete/:id" element={<PostDelete />} />
          <Route path="/posts/:id" element={<PostShow />} />
        </Routes>
      </div>
    </>
  );
};

export default App;
