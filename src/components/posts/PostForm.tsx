import { FC, useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { IFormInputs } from "../../app.interface";
import { Link } from "react-router-dom";
import "../../css/App.css";

interface Props {
  formTitle: string;
  values?: IFormInputs;
  onSubmit: (formValues: IFormInputs) => void;
}

const PostForm: FC<Props> = ({ formTitle, values, onSubmit }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInputs>({
    mode: "onChange",
  });

  const formSubmitHandler: SubmitHandler<IFormInputs> = (data: IFormInputs) => {
    onSubmit(data);
  };

  useEffect(() => {});

  return (
    <div className="streamForm">
      <form
        onSubmit={handleSubmit(formSubmitHandler)}
        className="form streamForm__inner"
      >
        <h3>{formTitle}</h3>

        <input
          {...register("title", { required: "Please enter a Title!" })}
          type="text"
          placeholder="Enter a Title"
          className="post-input"
          defaultValue={values?.title}
        />
        {errors?.title && (
          <div className="ui error message">{errors.title.message}</div>
        )}
        <input
          {...register("description", {
            required: "Please enter a Description!",
          })}
          type="text"
          placeholder="Enter a Description"
          className="post-input"
          defaultValue={values?.description}
        />
        {errors?.description && (
          <div className="ui error message">{errors.description.message}</div>
        )}
        <input
          {...register("postcontent", {
            required: "Please enter a Post Body!",
          })}
          type="text"
          placeholder="Enter a Post Body"
          className="post-input"
          defaultValue={values?.postcontent}
        />
        {errors?.postcontent && (
          <div className="ui error message">{errors.postcontent.message}</div>
        )}
        <button type="submit" className="custom-btn">
          Submit
        </button>
        <Link to="/" className="custom-link">
          Cancel
        </Link>
      </form>
    </div>
  );
};

export default PostForm;
