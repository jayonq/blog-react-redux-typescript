import _ from "lodash";
import { FC, useEffect } from "react";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import PostForm from "./PostForm";
import { IFormInputs } from "../../app.interface";
import { useParams, useNavigate } from "react-router-dom";

const PostEdit: FC = () => {
  const { fetchPost, editPost } = useActions();
  const { id } = useParams();
  const postId = Number(id);
  const post = useTypedSelector((state) => state.posts.items[postId]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchPost(Number(postId));
  }, []);

  const onSubmit = (formValues: IFormInputs) => {
    editPost(postId, formValues);
    navigate("/");
    navigate(0)
  };
  if (!post) {
    return <div>Loading...</div>;
  }
  return (
    <div>
      <PostForm
        formTitle={"Edit a Post"}
        values={{
          title: post.title,
          description: post.description,
          postcontent: post.postcontent,
        }}
        onSubmit={onSubmit}
      />
    </div>
  );
};

export default PostEdit;
