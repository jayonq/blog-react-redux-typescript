import _ from "lodash";
import { FC, useEffect } from "react";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { useParams, Link } from "react-router-dom";

const PostShow: FC = () => {
  const { fetchPost } = useActions();
  const { id } = useParams();
  const postId = Number(id);
  const post = useTypedSelector((state) => state.posts.items[postId]);

  useEffect(() => {
    fetchPost(Number(postId));
  }, []);

  if (!post) {
    return <div>Loading...</div>;
  }
  return (
    <section className="posts__main">
      <div className="container">
        <div className="posts__inner">
          <div className="post__list">
            <div className="post">
              <img src="https://picsum.photos/740/230/?random" alt="none" />
              <div className="cont">
                <h1 className="post-header">{post.title}</h1>
                <br />
                <h5 className="description">{post.description}</h5>
                <p>{post.postcontent}</p>
                <Link to="/" className="custom-link">
                  Back
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default PostShow;
