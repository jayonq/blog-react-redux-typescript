import { FC, useEffect, useReducer } from "react";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { Link, useLocation } from "react-router-dom";
import { Post } from "../../state/reducers/postReducer";
import _ from "lodash";

const PostList: FC = () => {
  const { fetchPosts } = useActions();
  const posts = useTypedSelector((state) => _.values(state.posts.items));
  // const [reducerValue, forceUpdate] = useReducer((x) => x + 1, 0);

  useEffect(() => {
    fetchPosts();
  }, []);

  const renderAdmin = (post: Post) => {
    // if (post.userId === this.props.currentUserId) {
    return (
      <div>
        <Link to={`/posts/edit/${post.id}`} className="custom-link">
          Edit
        </Link>
        <Link to={`/posts/delete/${post.id}`} className="custom-link">
          Delete
        </Link>
      </div>
    );
    // }
  };

  const renderList = () => {
    return posts.map((post: any) => {
      return (
        <div className="post" key={post.id}>
          <img src="https://picsum.photos/740/230/?random" alt="none" />
          <div className="cont">
            <Link to={`/posts/${post.id}`} className="post-header">
              {post.title}
            </Link>
            <p className="description">{post.description}</p>
            <p>{post.postcontent}</p>

            {renderAdmin(post)}
          </div>
        </div>
      );
    });
  };

  const renderCreate = () => {
    // if (this.props.isSignedIn) {
    return (
      <div style={{ textAlign: "right" }}>
        <Link to="/posts/new" className="custom-link">
          Create Post
        </Link>
      </div>
    );
    // }
  };

  return (
    <div>
      <section className="posts__main">
        <div className="container">
          <div className="posts__inner">
            <div className="post__list">{renderList()}</div>

            <aside className="posts__inner-aside">
              <div className="advertising">
                <img src="https://picsum.photos/330/200/?random" alt="none" />
                <div className="cont">
                  <h2>Edited Mauris</h2>
                  <p>
                    Edited Mauris neque quam, fermentum ut nisl vitae, convallis
                    maximus nisl. Sed mattis nunc id lorem euismod placerat.
                    Vivamus porttitor magna enim, ac accumsan tortor cursus at.
                    Phasellus sed ultricies mi non congue ullam corper.
                  </p>
                </div>
              </div>
            </aside>
          </div>
        </div>
        <div className="createPostBtn">{renderCreate()}</div>
      </section>
    </div>
  );
};

export default PostList;
