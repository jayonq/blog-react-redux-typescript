import { FC, useEffect } from "react";
import _ from "lodash";
import { useTypedSelector } from "../../hooks/useTypedSelector";
import { useActions } from "../../hooks/useActions";
import { useParams, useNavigate } from "react-router-dom";
import { Link } from "react-router-dom";
import Modal from "../Modal";

const PostDelete: FC = () => {
  const { fetchPost, deletePost } = useActions();
  const { id } = useParams();
  const postId = Number(id);
  const post = useTypedSelector((state) => state.posts.items[postId]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchPost(Number(postId));
  }, []);

  const renderActions = () => {
    return (
      <div className="modal-btn">
        <button
          onClick={() => {
            deletePost(postId);
            navigate("/");
          }}
          className="custom-link-negative"
        >
          Delete
        </button>
        <Link to="/" className="custom-link">
          Cancel
        </Link>
      </div>
    );
  };

  const renderContent = () => {
    if (!post) {
      return "Are you sure that you want to delete this stream?";
    }
    return `Are you sure that you want to delete the stream with title: ${post.title}?`;
  };
  return (
    <Modal
      title="Delete Post"
      content={renderContent()}
      actions={renderActions()}
      onDismiss={() => navigate("/")}
      // className="modal"
    />
  );
};

export default PostDelete;
