import { FC } from "react";
import { IFormInputs } from "../../app.interface";
import { useActions } from "../../hooks/useActions";
import { useNavigate } from "react-router-dom";
import PostForm from "./PostForm";

const PostCreate: FC = () => {
  const { createPost } = useActions();
  const navigate = useNavigate();

  const onSubmit = (formValues: IFormInputs) => {
    createPost(formValues);
    navigate("/");
    navigate(0);
  };

  return (
    <div>
      <PostForm formTitle={"Create a Post"} onSubmit={onSubmit} />
    </div>
  );
};

export default PostCreate;
