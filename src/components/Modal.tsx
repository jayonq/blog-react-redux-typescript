import { FC, useEffect } from "react";
import ReactDOM from "react-dom";

interface Props {
  title: string;
  content: string;
  actions: JSX.Element;
  onDismiss: () => void;
}

const Modal: FC<Props> = ({ title, content, actions, onDismiss }) => {
  return ReactDOM.createPortal(
    <div onClick={onDismiss} className="ui dimmer modals visible active">
      <div
        onClick={(e) => e.stopPropagation()}
        className="ui standard modal visible active"
      >
        <div className="header">{title}</div>
        <div className="content">{content}</div>
        <div className="actions">{actions}</div>
      </div>
    </div>,
    document.getElementById("root")!
  );
};

export default Modal;

// const container = document.getElementById("root")!;
// const root = createRoot(container);
