import { Component } from "react";
import { connect } from "react-redux";
import { signIn, signOut } from "../state/action-creators";
import { gapi } from "gapi-script";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useActions } from "../hooks/useActions";

interface Props {
  name: string;
  value: number;
}
interface ClassState {
  value: number;
}

class GoogleAuth extends Component<{}, any> {
  componentDidMount() {
    window.gapi.load("client:auth2", () => {
      gapi.client
        .init({
          clientId:
            "761621560788-plncku2qjgn2nmoh3ap3p9s976j3r90e.apps.googleusercontent.com",
          scope: "email",
          plugin_name: "Streamy",
        })
        .then(() => {
          // this.auth = gapi.auth2.getAuthInstance();
          this.onAuthChange(gapi.auth2.getAuthInstance().isSignedIn.get());
          gapi.auth2.getAuthInstance().isSignedIn.listen(this.onAuthChange);
        });
    });
  }

  onAuthChange = (isSignedIn: any) => {
    if (isSignedIn) {
      // this.props.signIn(gapi.auth2.getAuthInstance().currentUser.get().getId());
    } else {
      // this.props.signOut();
    }
  };

  onSignInClick = () => {
    // this.auth.signIn();
  };

  onSignOutClick = () => {
    // this.auth.signOut();
  };

  renderAuthButton() {
    // if (this.props.isSignedIn === null) {
    // return null;
    // } else if (this.props.isSignedIn) {
    return (
      <div className="header__box">
        <button onClick={this.onSignOutClick} className="custom-btn">
          Sign Out
        </button>
      </div>
    );
    // } else {
    // return (
    // <div className="header__box">
    // <button onClick={this.onSignInClick} className="custom-btn">
    // Sign In with Google
    // </button>
    // </div>
    // );
  }

  render() {
    return <div>{this.renderAuthButton()}</div>;
  }
}

// const mapStateToProps = (state) => {
//   return { isSignedIn: state.auth.isSignedIn };
// };

export default connect(null, { signIn, signOut })(GoogleAuth);
